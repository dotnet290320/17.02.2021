using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaitHandlersDemo
{
    class CustomerService
    {
        private int m_support_people = 0;
        private static Random r = new Random();
        // which event handler to use?
        
        public void NewSupportPersonArrived()
        {
            // support will be given only 
            Console.WriteLine("Another support person arrived");
            m_support_people++;
        }
        public void GiveSupportToCustomer()
        {
            Console.WriteLine("customer arrived");
            // do not accept customers if you have 3 or less support people
            m_support_people--;
            Console.WriteLine("Giving support");
            Thread.Sleep(r.Next(5000, 10000));
            m_support_people++;
        }

    }
}
