﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaitHandlersDemo
{
    class Program
    {

        static void Main(string[] args)
        {
            PageLoader page = new PageLoader();
            
            new Thread(() => { page.FireRequestToWebSite(); }).Start();
            new Thread(() => { page.ShowAmazingScrollBarsAnimation(); }).Start();
            new Thread(() => { page.MakeWindowAnimation(); }).Start();

            new Thread(() => { page.PageLoad(); }).Start();
            Console.ReadLine();
        }
    }
}
