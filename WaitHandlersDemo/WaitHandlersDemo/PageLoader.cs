﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaitHandlersDemo
{
    public class PageLoader
    {
        private AutoResetEvent gate = new AutoResetEvent(false);

        public void MakeWindowAnimation()
        {
            Console.WriteLine("MakeWindowAnimation arrived...");
            gate.WaitOne();
            Console.WriteLine("Making window animation .... ^%$@$#");
            gate.Set();
        }
        public void ShowAmazingScrollBarsAnimation()
        {
            Console.WriteLine("ShowAmazingScrollBarsAnimation arrived...");
            gate.WaitOne();
            Console.WriteLine("Show scroll bars animation ....");
      
        }
        public void FireRequestToWebSite()
        {
            Console.WriteLine("FireRequestToWebSite arrived...");
            gate.WaitOne();
            Console.WriteLine("Fire request to web site");
            gate.Set();
        }
        public void PageLoad()
        {
            Console.WriteLine("Loading page...");
            Thread.Sleep(2000);
            Console.WriteLine("Page loaded");
            gate.Set();
        }
    }
}
